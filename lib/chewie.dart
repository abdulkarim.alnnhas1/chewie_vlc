library chewie;

export 'src/chewie_player.dart';
export 'src/chewie_progress_colors.dart';
export 'src/cupertino/cupertino_controls.dart';
export 'src/models/index.dart';
export 'package:flutter_vlc_player/flutter_vlc_player.dart';